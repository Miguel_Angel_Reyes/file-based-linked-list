#ifndef FILELINKEDLIST
#define FILELINKEDLIST
#include <iostream>

template<typename T>
class FileLinkedList {
  FileLinkedList(const FileLinkedList<T> &that) = delete;
  FileLinkedList<T> operator=(const FileLinkedList<T> &that) = delete;
  int len;
  int first_free;
  FILE * afile;
 public:
  // index is based on file
  int toLoc(int index) const {
    if(index == -1){
      return (sizeof(int)*2);
    } else {
      return (index*(sizeof(T)+(sizeof(int)*2)))+(sizeof(int)*4)+sizeof(T);
    }
  }
  // index is based on file
  int toIndex(int index) {
    return (index-((sizeof(int))*2))/(sizeof(T)+(sizeof(int)*2));
  }
  // index is based of file
  int readPrev(FILE * f,int index) const {
    int tmp;
    fseek(f,toLoc(index),SEEK_SET);
    fread(&tmp,sizeof(int),1,f);
    return tmp;
  }
  int readNext(FILE * f,int index) const {
    int tmp;
    fseek(f,toLoc(index)+sizeof(int),SEEK_SET);
    fread(&tmp,sizeof(int),1,f);
    return tmp;
  }
  T readData(FILE * f,int index) const {
    int tmp;
    fseek(f,toLoc(index)+(sizeof(int)*2),SEEK_SET);
    fread(&tmp,sizeof(T),1,f);
    return tmp;
  }
  void writePrev(FILE * f,int index, int value){
    fseek(f,toLoc(index),SEEK_SET);
    fwrite(&value,sizeof(int),1,f);
  }
  void writeNext(FILE * f,int index, int value){
    fseek(f,toLoc(index)+sizeof(int),SEEK_SET);
    fwrite(&value,sizeof(int),1,f);
  }
  void writeData(FILE * f,int index, const T t){
    fseek(f,toLoc(index)+(sizeof(int)*2),SEEK_SET);
    fwrite(&t,sizeof(T),1,f);
  }
  // given index of array return index of file
  int toFileIndex(FILE * f,int index) const {
    int tmp = readNext(f,-1);
    for(int i = 0;i < index;i++){
      tmp = readNext(f,tmp);
    }
    return tmp;
  }
  int readFirstFree(FILE * f){
    return first_free;
  }
  void writeFirstFree(FILE * f, int value){
  	first_free=value;
	}
  void addFF(FILE * f,int index){
	writeNext(f,index,readFirstFree(f));
	writeFirstFree(f,index);  
}
  int readLength(FILE * f) const {
    int tmp;
    fseek(f,0,SEEK_SET);
    fread(&tmp,sizeof(int),1,f);
    return tmp;
  }
  void writeLength(FILE * f, int value) {
    fseek(f,0,SEEK_SET);
    fwrite(&value,sizeof(int),1,f);
  }
      typedef T value_type;

    class const_iterator {
      int index;
      FILE * iterfile;
    public:
      int toLoc(int index){
        if(index == -1){
          return (sizeof(int)*2);
        } else {
          return (index*(sizeof(T)+(sizeof(int)*2)))+(sizeof(int)*4)+sizeof(T);
        }
      }
      void writeNext(FILE * f,int index, int value){
        fseek(f,toLoc(index)+sizeof(int),SEEK_SET);
        fwrite(&value,sizeof(int),1,f);
      }
      int readPrev(FILE * f,int index){
        int tmp;
        fseek(f,toLoc(index),SEEK_SET);
        fread(&tmp,sizeof(int),1,f);
        return tmp;
      }
      int readNext(FILE * f,int index){
        int tmp;
        fseek(f,toLoc(index)+sizeof(int),SEEK_SET);
        fread(&tmp,sizeof(int),1,f);
        return tmp;
      }
      T readData(FILE * f,int index){
        T tmp;
        fseek(f,toLoc(index)+(sizeof(int)*2),SEEK_SET);
        fread(&tmp,sizeof(T),1,f);
        return tmp;
      }
      const_iterator(int i,FILE *f){
        index = i;
        iterfile = f;
      }
      const_iterator(const const_iterator &i){
        index = i.index;
        iterfile = i.iterfile;
      }
      T operator*(){
        return readData(iterfile,index);
      }
      bool operator==(const const_iterator &i) const {
        return (index==i.index) && (iterfile==i.iterfile);
      }
      bool operator!=(const const_iterator &i) const {
        return !((index==i.index) && (iterfile==i.iterfile));
      }
      const_iterator &operator=(const const_iterator &i){
        index = i.index;
        iterfile = i.iterfile;
        return *this;
      }
      const_iterator &operator++() {
        index = readNext(iterfile,index);
        return *this;
      }
      const_iterator &operator--(){
        index = readPrev(iterfile,index);
        return *this;
      }
      const_iterator operator++(int){
        const_iterator ret(*this);
        index = readNext(iterfile,index);
        return ret;
      }
      const_iterator operator--(int){
        const_iterator ret(*this);
        index = readPrev(iterfile,index);
        return ret;
      }

      friend class FileLinkedList;
    };

    FileLinkedList(const std::string &fname) {
      afile = fopen(fname.c_str(),"rb+");
      if(afile==nullptr) {
        afile = fopen(fname.c_str(),"wb+");
        len=0;
		first_free=0;
        writePrev(afile,-1,-1);
        writeNext(afile,-1,-1);
        writeFirstFree(afile,-1);
        writeLength(afile,len);
      } else {
        len = readLength(afile);
    	fseek(afile,sizeof(int),SEEK_SET);
		fread(&first_free,sizeof(int),1,afile); 
	 }
    }

    template<typename I>  // The type I will be an iterator.
      FileLinkedList(I begin,I end,const std::string &fname) {
      afile = fopen(fname.c_str(),"wb+");
      len=0;
	  writeLength(afile,len);
      writeFirstFree(afile,-1);
      writePrev(afile,-1,-1);
      writeNext(afile,-1,-1);
      for(auto i = begin;i!=end;i++){
        push_back(*i);
      }
    }
    ~FileLinkedList(){
      writeLength(afile,len);
	  fseek(afile,sizeof(int),SEEK_SET);
	  fwrite(&first_free,sizeof(int),1,afile);
      fflush(afile);
      fclose(afile);
    }
    void push_back(const T &t){
      if(readFirstFree(afile) != -1){
        int locIndex = readFirstFree(afile);
        int newFree = readNext(afile,locIndex);
        writeFirstFree(afile,newFree);
        int prevIndex = readPrev(afile,-1);
        writeNext(afile,locIndex,-1);
        writePrev(afile, locIndex, prevIndex);
        writeNext(afile, prevIndex, locIndex);
        writePrev(afile,-1,locIndex);
        writeData(afile,locIndex,t);
        len++;
        writeLength(afile,len);
      } else {
        int locIndex = len;
        int prevIndex = readPrev(afile,-1);
        writeNext(afile,locIndex,-1);
        writePrev(afile, locIndex, prevIndex);
        writeNext(afile, prevIndex, locIndex);
        writePrev(afile,-1,locIndex);
        writeData(afile,locIndex,t);
        len++;
      }
    }
    void pop_back() {
	int loc = readPrev(afile,readPrev(afile,-1));
      int newFree = readPrev(afile,-1);
      	 writeNext(afile,loc , -1);
	 writePrev(afile,-1,loc);
      addFF(afile,newFree);
      len--;
    }
    int size() const {
      return len;
    }
    void clear() {
      len = 0;
      writeLength(afile,len);
      writePrev(afile,-1,-1);
      writeNext(afile,-1,-1);
       
    }
    const_iterator insert(const_iterator position, const T &t) {
      if(readFirstFree(afile) != -1){
        int locIndex = readFirstFree(afile);
        int newFree = readNext(afile,locIndex);
        writeFirstFree(afile,newFree);
        int prev = readPrev(afile,position.index);
        writeNext(afile,prev,locIndex);
        writePrev(afile,position.index,locIndex);
        writePrev(afile,locIndex,prev);
        writeNext(afile,locIndex,position.index);
        writeData(afile,locIndex,t);
        len++;
        writeLength(afile,len);
        return position;
      } else {
        int locIndex = len;
        int prev = readPrev(afile,position.index);
        writeNext(afile,prev,locIndex);
        writePrev(afile,position.index,locIndex);
        writePrev(afile,locIndex,prev);
        writeNext(afile,locIndex,position.index);
        writeData(afile,locIndex,t);
        len++;
        return position;
      }
    }
    T operator[](int index) const{
      return readData(afile,toFileIndex(afile,index));
    }
    const_iterator erase(const_iterator position) {
      const_iterator ret = position;
		++ret;	  	
	if(len==1){
        writePrev(afile,-1,-1);
        writeNext(afile,-1,-1);
        addFF(afile,position.index);
      } else if(readNext(afile,position.index) == -1) {
        int newLoc = readPrev(afile,position.index);
        writeNext(afile,newLoc,-1);
        writePrev(afile,-1,newLoc);
        addFF(afile,position.index);
      } else if(readPrev(afile,position.index) == -1) {
        int newLoc = readNext(afile,position.index);
        writePrev(afile,newLoc,-1);
        writeNext(afile,-1,newLoc);
        addFF(afile,position.index);
      } else {
        int prev = readPrev(afile,position.index);
        int next = readNext(afile,position.index);
        writeNext(afile,prev,next);
        writePrev(afile,next,prev);
        addFF(afile,position.index);
      }
        len--;
      return ret;
    }
    void set(const T &value,int index) {
      writeData(afile,toFileIndex(afile,index),value);
    }
    void set(const T &value,const_iterator position){
      writeData(afile,position.index,value);
    }

    const_iterator begin(){
      return const_iterator(readNext(afile,-1),afile);
    }
    const_iterator begin() const { 
      return const_iterator(readNext(afile,-1),afile);
    }
    const_iterator end() {
      return const_iterator(-1,afile);
    }
    const_iterator end() const {
      return const_iterator(-1,afile);
    }
    const_iterator cbegin() const {
      return const_iterator(readNext(afile,-1),afile);
    }
    const_iterator cend() const {
      return const_iterator(-1,afile);
    }
  };
#endif
